from time import sleep

from django.test import TestCase

from .models import Candidate


class ModelTests(TestCase):

    def test_1(self):
        candidate = Candidate(first_name='Mark', last_name='Lit')
        candidate.save()

        sleep(0.8) # 800 milliseconds

        candidate = Candidate.objects.get(first_name='Mark')
        self.assertEqual(candidate.first_name, 'Mark')

    def test_2(self):
        self.test_1()

    def test_3(self):
        self.test_1()

    def test_4(self):
        self.test_1()

    def test_5(self):
        self.test_1()

    def test_6(self):
        self.test_1()

    def test_7(self):
        self.test_1()
        self.assertTrue(False, 'False positive')

    def test_8(self):
        self.test_1()

    def test_9(self):
        self.test_1()

    def test_10(self):
        self.test_1()

    def test_11(self):
        self.test_1()

    def test_12(self):
        self.test_1()